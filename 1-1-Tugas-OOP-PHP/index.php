<?php
trait Hewan{
    public $nama;
    public $nilai = 50;
    public $jumlahKaki;
    public $keahlian;
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian} ";
    }

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}, ";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama} ";
        $this->nilai = $this->nilai - ($hewan->attackPower / $this->defencePower);
    }

    public function getInfo()
    {
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Nilai : {$this->nilai}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
        echo "<br>";
        $this->atraksi();
        echo "<br>";
    }

    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($string)
    {
        $this->nama = $string;
        $this->jumlahKaki = 2;
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
    public function getInfoHewan()
    {
        echo "Hewan : Elang ";
        $this->getInfo();
    }
}

class Harimau extends Fight{
    public function __construct($string)
    {
        $this->nama = $string;
        $this->jumlahKaki = 4;
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    public function getInfoHewan()
    {
        echo "Hewan : Harimau ";
        $this->getInfo();
    }
}

$elang = new Elang("Elang");
$harimau = new Harimau("Harimau");


$harimau->serang($elang);
echo "<br> <br>";

$elang->serang($harimau);
echo "<br> <br>";

$elang->getInfoHewan();
echo "<br> <br>";

$harimau->getInfoHewan();
echo "<br> <br>";

